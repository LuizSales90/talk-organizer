package filler;

public final class TalkFactory {
	
	public static Talk createTalk(String name, int duration) {			
		Talk talk = new Talk();
		talk.name = name;
		talk.duration = duration;
		return talk;		
	}

}
