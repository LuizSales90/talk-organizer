package filler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Track {
	
	public String name;
	private List<Talk> talks = new ArrayList<>();
	private int duration;
	
	public Track(String name) throws IOException, Exception {	
		this.name = name;
		this.duration = 390;		
	}
	
	public void setTalks(ReadTxt reader) throws IOException, Exception {
		for(int i=0; this.duration!=0;i++) {
				  if(!(reader.talks60min.isEmpty()) && this.duration>= 60 ) {
					this.talks.add(reader.talks60min.get(i));
					this.duration = this.duration - reader.talks60min.get(i).duration;
					reader.talks60min.remove(i);
					i--;
				}else {
					if(this.duration>=45 && !(reader.talks45min.isEmpty())) {
						this.talks.add(reader.talks45min.get(i));
						this.duration = this.duration - reader.talks45min.get(i).duration;
						reader.talks45min.remove(i);
						i--;
					}else
						if(this.duration>=30 && !(reader.talks30min.isEmpty())) {
							this.talks.add(reader.talks30min.get(i));
							this.duration = this.duration - reader.talks30min.get(i).duration;
							reader.talks30min.remove(i);
							i--;
					}
				}
			}
		}
	
	public void getTrack() {
		System.out.println(this.name);
		for(int i=0; i< this.talks.size();i++) {
			System.out.println(this.talks.get(i).name);
		}		
	}
}

