package filler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ReadTxt {
	
	public List<Talk> talks30min = new ArrayList<>();
	public List<Talk> talks45min = new ArrayList<>();
	public List<Talk> talks60min = new ArrayList<>();
	private static ReadTxt instance;
	
	private ReadTxt() throws IOException, Exception {
	}
	
	
	public static synchronized ReadTxt getInstace() throws IOException, Exception {
		if(instance==null) {
			instance = new ReadTxt();
		}
		return instance;
	}
	
	public void readNclassifyTalks() throws IOException, Exception {
		InputStream in = getClass().getResourceAsStream("/filler/proposals.txt");
		BufferedReader input = new BufferedReader(new InputStreamReader(in));		
		String aux;
		while(input.ready()) {
			aux = input.readLine();
			for(int i=0; i< aux.length();i++ ) {			
				if(Character.isDigit(aux.charAt(i)))
				{					
					if(aux.charAt(i)=='3') {
						talks30min.add(TalkFactory.createTalk(aux, 30));
					}else if(aux.charAt(i)=='4') {
						talks45min.add(TalkFactory.createTalk(aux, 45));
					}else if(aux.charAt(i)=='6') {
						talks60min.add(TalkFactory.createTalk(aux, 60));
					}					
				}
			}
		}		
	}
	
	public void getTalks() {
		System.out.println("Palestras de 30min:");
		for(int i=0;i<talks30min.size();i++) {
		System.out.println(talks30min.get(i).name);	
		}
		
		System.out.println("Palestras de 45min:");
		for(int i=0;i<talks45min.size();i++) {
		System.out.println(talks45min.get(i).name);	
		}
		
		System.out.println("Palestras de 60min:");
		for(int i=0;i<talks60min.size();i++) {
		System.out.println(talks60min.get(i).name);	
		}
	}	

}
